import pytest

from fastapi import FastAPI
from fastapi.testclient import TestClient

from app import app

client = TestClient(app)

correct_user = {'grant_type':'password',
                'username': 'user1', 
                'password':'user1'}

wrong_users = [{'username': 'test', 
                'password':'test', 
                'grant_type':'password'},
                {'username': 'user1', 
                'password':'test', 
                'grant_type':'password'}]


def test_read_user_data_without_login():
    responce = client.get("/user")
    assert responce.status_code == 401
    assert responce.json() == {
        'detail': 'Not authenticated'
    }

def test_incorrect_login_data():
    responce = client.post('/login', json={'username':'user1', 'password':'user1'})
    assert responce.status_code == 422


def test_login_wrong_username():
    responce = client.post('/login', data=wrong_users[0])
    assert responce.status_code == 401
    assert responce.json() == {
        'detail':'Invalid username'
    }

def test_login_wrong_password():
    responce = client.post('/login', data=wrong_users[1])
    assert responce.status_code == 401
    assert responce.json() == {
        'detail':'Invalid password'
    }

def test_correct_login():
    responce = client.post('/login', data=correct_user)
    assert responce.status_code == 200
    
def test_get_user():
    access_token = client.post('/login', data=correct_user).json()['access_token']
    responce = client.get('/user', headers={'Authorization':f'Bearer {access_token}'})
    assert responce.status_code == 200
    assert responce.json() == {
    "user_login": "user1",
    "firstname": "Иван",
    "lastname": "Иванов",
    "jobtitle": "работа1",
    "salary": 10000,
    "promotion_date": "2024-06-01"
    }