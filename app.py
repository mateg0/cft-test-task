from datetime import date
from typing import Annotated

from auth.auth import Auth
from db import crud, schemas
from database import SessionLocal
import ssl

from fastapi import FastAPI, HTTPException, status, Depends
from fastapi.security import HTTPBearer, HTTPBasic, HTTPAuthorizationCredentials
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

ssl._create_default_https_context = ssl._create_unverified_context

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


security = HTTPBearer()
auth_handler = Auth()


@app.post('/login')
async def login(form_data: Annotated[OAuth2PasswordRequestForm, Depends()], 
    db: Session = Depends(get_db)
    ):
    user = await crud.get_auth_data(db, form_data.username)
    if user is None:
        raise HTTPException(status_code=401, detail='Invalid username')
    if not auth_handler.verify_password(form_data.password, user.password):
        raise HTTPException(status_code=401, detail='Invalid password')
    access_token = auth_handler.encode_token(user.login)
    return {"access_token": access_token, "token_type": "bearer"}


@app.get('/user', response_model=schemas.User)
def get_user_by_login(token: Annotated[str, Depends(oauth2_scheme)], db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    if (auth_handler.decode_token(token)):
        user = auth_handler.decode_token(token)
        db_user = crud.get_user_by_login(db, user)
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        return db_user
    return credentials_exception
