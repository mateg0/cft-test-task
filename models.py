from sqlalchemy import Boolean, Column, Numeric, String, Date
from sqlalchemy.orm import relationship, declarative_base

Base = declarative_base()

class User_DB(Base):
    __tablename__ = "user"

    user_login = Column(String, primary_key=True)
    firstname = Column(String)
    lastname = Column(String)
    jobtitle = Column(String)
    salary = Column(Numeric(10, 2))
    promotion_date = Column(Date)


class Login_DB(Base):
    __tablename__ = "login"

    login = Column(String, primary_key=True)
    password = Column(String(64), nullable=False)

