from pydantic import BaseModel, validator, root_validator
from datetime import date


class User(BaseModel):
    user_login: str
    firstname: str
    lastname: str
    jobtitle: str
    salary: float
    promotion_date:date

    class Config:
        orm_mode = True

class AuthModel(BaseModel):
    login: str
    password: str

    class Config:
        orm_mode = True

class SingUpModel(BaseModel):
    user_login:str
    password:str
    firstname: str
    lastname: str
    jobtitle: str
    salary: float
    promotion_date:date

    class Config:
        orm_mode = True
