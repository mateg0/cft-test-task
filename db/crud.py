from sqlalchemy.orm import Session
from sqlalchemy import insert
from db import schemas
import models


def get_user_by_login(db: Session, login: str):
    return db.query(models.User_DB).filter(models.User_DB.user_login == login).first()

async def get_auth_data(db: Session, login: str):
    return db.query(models.Login_DB).filter(models.Login_DB.login == login).first()

def put_user(db: Session, data: schemas.SingUpModel):
    login_data = {'login':data.user_login, 'password':data.password}
    user_data = {'user_login':data.user_login ,'firstname':data.firstname, 
                             'lastname':data.lastname, 
                             'jobtitle':data.jobtitle, 
                             'salary':data.salary, 
                             'promotion_date':data.promotion_date}
    result1 = db.execute(insert(models.Login_DB).values(login_data))
    result2 = db.execute(insert(models.User_DB).values(user_data))
    db.commit()
    return {'login_add': result1, 'data_add': result2}
   
   