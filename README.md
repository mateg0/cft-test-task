# CFT Test Task

Решение тестового задания для ЦФТ ШИФТ

## Установка

1. Для установки необходимо установить poetry:

>*https://python-poetry.org/docs/*

2. Далее необходимо клонировать репозиторий на устройство:

>*git clone https://gitlab.com/mateg0/cft-test-task*

3. Далее необходимо перейти в директорию репозитория и ввести команду:

>*cd cft-test-task*

>*poetry install*

## Запуск проекта

Для запуска проекта необходимо ввести команду:

>*poetry run python main.py*

## Как пользоваться

Для использования проекта необходимо:

1. Перейти по ссылке *http://127.0.0.1:8000/docs*
2. Нажать кнопку __Authorize__
3. Ввести один из следующих вариантов:

    - username: user1, password: user1
    - username:user2, password: user2
4. После этого необходимо закрыть окно авторизации, развернуть GET-запрос /user, нажать __Try it out__ и  __Execute__

В результате Вы увидите данные авторизованного пользователя

## Тестирование

Для запуска теста необходимо ввести команду:

>*poetry run pytest test.py*

